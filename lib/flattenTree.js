const lodashFlattenDeep = require('lodash/fp/flattenDeep');

// we need to do some type conversion
const convertType = (type) => {
  if (type === 'integer') return 'number';

  return type;
};

const constructKey = (key, trigger) => `${key}-${trigger}-`;

const createFlatQuestion = (key, entry) => {
  return {
    id: key + entry.id,
    type: convertType(entry.type),
    text: entry.question
  };
};

/*
 create set of questions from group
 */
const createFromGroup = (key, group) => {
  return flattenQuestions(group.questions, constructKey(key, group.trigger));
};

// flatten out each of the groups
const flattenGroups = (groups, key) => {

  // check each group
  const filteredGroups = groups.filter((group) => {
    const countOfTriggers = (key.match(/YES-/g) || []).length;
    // we need to output the questions if a top level group - note this is a conflict between requirements and examples
    return (group.trigger === 'YES' || (group.trigger === 'NO') && countOfTriggers === 1);
  });

  return filteredGroups.map((group) => createFromGroup(key, group));
};

// action each of the questions
const flattenQuestions = (questions, key) => {
  let result = [];

  // check all questions
  questions.forEach((entry) => {

    // add the entry
    result.push(createFlatQuestion(key, entry));

    // check for groups
    if (entry.hasOwnProperty('groups')) { // it's a group
      // recurse through all the groups
      result = result.concat(flattenGroups(entry.groups, key + entry.id));
    }
  });

  return lodashFlattenDeep(result);
};

module.exports = (questionset, answers) => {
  // guards
  if (questionset === null || questionset === undefined || answers === undefined || answers === null) {
    return [];
  }

  /// ignoring answers as they don't seem to be involved
  return flattenQuestions(questionset, '');
};
