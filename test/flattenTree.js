const test = require('ava');
const flattenTree = require('../lib/flattenTree');

test('should behave when both questions and answers null', t => {
  t.deepEqual(flattenTree(null, null), []);
});

test('should behave when questions null', t => {
  t.deepEqual(flattenTree(null, {}), []);
});

test('should behave when answers null', t => {
  t.deepEqual(flattenTree({}, null), []);
});

test('should behave when both questions and answers undefined', t => {
  t.deepEqual(flattenTree(undefined, undefined), []);
});

test('should behave when questions null', t => {
  t.deepEqual(flattenTree(undefined, {}), []);
});

test('should behave when answers null', t => {
  t.deepEqual(flattenTree({}, undefined), []);
});

test('should produce one response with one question', t => {
  let questionSet = [{
    "id": "question1",
    "type": "text",
    "question": "Please answer this question"
  }];
  let answerSet = {
    "question1": "YES"
  }
  const expectedResponse = [{
    "id": "question1",
    "type": "text",
    "text": "Please answer this question"
  }];

  t.deepEqual(flattenTree(questionSet, answerSet), expectedResponse);
});

test('should produce one response with one question replacing integer with number', t => {
  let questionSet = [{
    "id": "question1",
    "type": "integer",
    "question": "Please answer this question"
  }];
  let answerSet = {
    "question1": "YES"
  }
  const expectedResponse = [{
    "id": "question1",
    "type": "number",
    "text": "Please answer this question"
  }];

  t.deepEqual(flattenTree(questionSet, answerSet), expectedResponse);
});

test('should produce two responses with two questions', t => {
  let questionSet = [{
    "id": "question1",
    "type": "integer",
    "question": "Please answer this question"
  }, {
    "id": "question2",
    "type": "text",
    "question": "Do you have a bmw"
  }];
  let answerSet = {
    "question1": "YES",
    "question2": "NO"
  }
  const expectedResponse = [{
    "id": "question1",
    "type": "number",
    "text": "Please answer this question"
  }, {
    "id": "question2",
    "type": "text",
    "text": "Do you have a bmw"
  }];

  t.deepEqual(flattenTree(questionSet, answerSet), expectedResponse);
});

test('should produce answer set from full question set', t => {
  const questionSet = require('../examples/questionset.json');
  const answerSet = require('../examples/answers.json');
  const expectedOutput = require('../examples/expectedOutput.json');

  t.deepEqual(flattenTree(questionSet, answerSet), expectedOutput);
});
